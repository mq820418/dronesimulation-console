package dronesimulation;
import java.util.Scanner;
import java.io.FileWriter;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;
import java.io.*;

public class DroneInterface {
    private Scanner s;
    private DroneArena myArena;
    private ConsoleCanvas c;
    JFileChooser chooser = new JFileChooser();
    FileFilter filter = new FileFilter() {
    	
    	public boolean accept(File f) {
    	if (f.getAbsolutePath().endsWith(".txt")) 
    		return true;
    	if (f.isDirectory()) 
    		return true;
    	
    	return false;
    	}
    	
    	public String getDescription() {
    	   return "txt";
    	}
    };

    
    public void doDisplay(){
       ConsoleCanvas c = new ConsoleCanvas(myArena.getX(),myArena.getY());
       myArena.ShowDrones(c);
       System.out.println(c.toString());
    }
    
    public void newArena() {
    	System.out.print("Enter the size of the arena (x,y)");
    	int x = 0;
    	int y = 0;
    	x = s.nextInt();
    	y = s.nextInt();
    	myArena = new DroneArena(x,y);
    	
    }
    
    public void open() {
    	chooser.setFileFilter(filter);
    	int returnVal = chooser.showOpenDialog(null);
    	if (returnVal == JFileChooser.APPROVE_OPTION) {
    		File selFile = chooser.getSelectedFile();
    		System.out.println("You chose to open this file: " + selFile.getName());
    		if(selFile.isFile()){ 
    			try {
        			Scanner read = new Scanner(selFile);
        			myArena = new DroneArena(read.nextInt(), read.nextInt());
        			while (read.hasNextLine()) {
        				myArena.fillWithDrones(read.nextInt(), read.nextInt(), myArena);
        			}
        			read.close();
        			
        		}
        		catch(Exception e) {
        			e.getStackTrace();
        		}
    		}
    	}
    }
    
    public void save() {
    	chooser.setFileFilter(filter);
    	int returnVal = chooser.showSaveDialog(null);
    	if (returnVal == JFileChooser.APPROVE_OPTION) {
    		File selFile = chooser.getSelectedFile();
    		File currDir = chooser.getCurrentDirectory();
    		System.out.println("You chose to save into file: "+ selFile.getName() + " in the dir " + currDir.getAbsolutePath());
    		try {
    			FileWriter writer = new FileWriter(selFile);
    			selFile.createNewFile();
    			writer.write(myArena.x + " " + myArena.y + "\n");
    			writer.write(myArena.getAllDrones());
    			writer.close();
    			
    		}
    		catch(Exception e) {
    			e.getStackTrace();
    		}
    	}
    }
    
   
    public DroneInterface() {
    	 s = new Scanner(System.in);			// set up scanner for user input
    	 myArena = new DroneArena(5, 5);	// create arena of size 20
         interaction();				
    }
    
    
    public void interaction() {
    	char ch = ' ';
        do {
        	System.out.print("Enter (A)dd drone, get (I)nformation, print canvas(D), (M)oves drones, Move drones 10 times(Z), add (N)ew arena, (S)ave arena, (L)oad arena or e(X)it > ");
        	ch = s.next().charAt(0);
        	s.nextLine();
        	switch (ch) {
    			case 'A' :
    			case 'a' :
        					myArena.addDrone(myArena);	// add a new drone at random position to arena
        					break;
        		case 'I' :
        		case 'i' :
        					System.out.print(myArena.toString());  //Print out arena size as well as all drone coordinates
            				break;
        		case 'd' :
        		case 'D' :
        			        this.doDisplay();    //Draw the arena with the drones
        			        break;
        		case 'M' :
        		case 'm' : 
        			        DroneArena.moveAllDrones();   //move all the drones
        			        this.doDisplay();
        			        break;
        		case 'Z' :
        		case 'z' :
        			       for(int i = 0; i<=10; i++) {        //Move drones 10 times
        			    	   DroneArena.moveAllDrones();
        			    	   this.doDisplay();
        			    	   try {
        			    	   Thread.sleep(400); //sleep for animation
        			    	   } catch (InterruptedException ie) {
        			    		   Thread.currentThread().interrupt();
        			    	   }
        			    	   
        			       }
        			       break;
        			       
        		case 'N' :
        		case 'n' :
        			       newArena();   //create new arena
        			       break;
        			        
        	    case 'x' : 	ch = 'X';				// when X detected program ends
        					break;
        					
        	    case 'S' :
        	    case 's' :
        	    	       save();         //save arena with drones
        	    	       break;
        	    
        	    case 'L' :
        	    case 'l' :
        	    	       open();    //load saved arenas with their drones
        	    	       break;
        	    	       
        	}
    		} while (ch != 'X');	
        s.close();
    }
    

}

