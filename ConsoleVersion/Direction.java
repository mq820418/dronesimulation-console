package dronesimulation;

import java.util.Random;
import java.io.*;

public enum Direction{
   North,
   East,
   South,
   West;
   
   public static Direction getRandomDirection() {
       Random random = new Random();
       return values()[random.nextInt(values().length)];
   }
   
   public static Direction getNextDirection(Direction d) {
	   switch(d) {
	   case North:
		   d = East;
		   break;
	   
	   case East:
		   d = South;
		   break;
	   
	   case South:
		   d = West;
		   break;
	   
	   case West:
		   d = North;
		   break;
	   }
	   return d;
		   
   }
}
