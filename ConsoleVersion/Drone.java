package dronesimulation;
import java.io.*;

public class Drone {
   private int x;
   private int y;
   private static int num = 0;
   private int num_id;
   private Direction movement;
   boolean test;
   public DroneArena arena;
   

   public Drone(int inp1, int inp2, Direction movement, DroneArena arena){
       
	   x = inp1;
	   y = inp2;
	   this.arena = arena;
	   this.movement = movement;
       this.num_id = num;
       num++;
   }
   
   public void setX(int x) {
	   this.x = x;
   }
   
   public void setY(int y) {
	   this.y = y;
   }
   
   public int getX() {
	   return this.x;
   }
   
   public int getY() {
	   return this.y;
   }
   
   public int get_num_id() {
	   return this.num_id;
   }
   
   public void DisplayDrone(ConsoleCanvas c) {
	   c.showlt(this.x,this.y);
   }
   
   public void tryToMove() {
       int i =0;
       int error = 0;
	   while(i!=1) {
		   test = DroneArena.canMoveHere(this.x, this.y, arena.getX(), arena.getY(), this.movement);
		   if (test == false) {
			   this.movement = Direction.getRandomDirection();
			   error++;
			  
		   }
		   if(error >= 10) {
			   i++;
		   }
		   if( test == true ) {
			   switch(this.movement) {
			   case North:
				   this.x = this.x-1;
				   break;
			   case East:
				   this.y = this.y+1;
				   break;
			   case South:
				   this.x = this.x + 1;
				   break;
			   case West:
				   this.y = this.y-1;
				   break;
			   }
		    i++;
		   }
		   
	   }
   }
   public boolean isHere(int x, int y) {
	   if (this.x == x && this.y == y) {
		   return true;
	   } else {
		   return false;
	   }
   }
   
   public String toString() {
	  String res = "Drone " + get_num_id() + " is at x = " + x +" and y = " + y + " heading " + this.movement;
	  return res; 
   }
}
