package dronesimulation;
import java.io.*;

public class ConsoleCanvas implements Serializable{
   String[][] Arena;
   int x;
   int y;
   
   public ConsoleCanvas(int x, int y){
	   this.x = x+2;
	   this.y = y+2;
	   Arena = new String[this.x][this.y];
	   for(int i = 0; i<this.x ; i++) {
		   for(int j = 0; j<this.y; j++) {
			   if (i == 0 || i == this.x-1) {
				   Arena[i][j] = "#";
			   }else {
				   if(j!=0 && j!=this.y-1) {
					   Arena[i][j] = " ";
				   }else if(j == 0 || j == this.y-1){
					   Arena[i][j] = "#";
				   }
			   }
		   }
	   }
   }
   
   public void showlt(int x, int y) {
	   Arena[x+1][y+1] = "D";
   }
   
   public String toString() {
	   String res = "";
	   for(int i =0; i<x; i++) {
		   res = res + "\n";
		   for (int j = 0; j<y; j++) {
			   res = res + Arena[i][j];
		   }
		   
	   }
	   return res;
   }
   
   
}
