package dronesimulation;

import java.util.Random;
import java.util.ArrayList;
import java.io.*;

public class DroneArena{
  public int x;
  public int y;
  Random randomgenerator;
  private int rand_X;
  private int rand_Y;
  Drone d;
  private static ArrayList <Drone> Drones;
 
  
  DroneArena(int x, int y){
	  randomgenerator = new Random();
	  Drones = new ArrayList<Drone>();
	  this.x = x;
	  this.y = y;
	  
  }
  public void addDrone(DroneArena arena){
	  this.rand_X= randomgenerator.nextInt((int)this.x);
	  this.rand_Y= randomgenerator.nextInt((int)this.y);
	  while(getDroneAt(rand_X, rand_Y) != null) {
		  this.rand_X= randomgenerator.nextInt((int)this.x);
		  this.rand_Y= randomgenerator.nextInt((int)this.y);
	  }
	  Drone d = new Drone(rand_X, rand_Y, Direction.getRandomDirection(),arena);
	  Drones.add(d);
  }
  
  public void fillWithDrones(int x, int y, DroneArena arena) {
	  Drone d = new Drone(x,y,Direction.getRandomDirection(),arena);
	  Drones.add(d);
  }

  
 
  public void ShowDrones(ConsoleCanvas c) {
	  for(Drone element : Drones) {
		  element.DisplayDrone(c);
	  }
  }
  
  public static void moveAllDrones() {
	  for(Drone element : Drones) {
		  element.tryToMove();
	  }
  }
  
  public static boolean canMoveHere(int x, int y,int arenaX, int arenaY, Direction movement) {
	 
	  boolean result = false;
	  switch(movement) {
	      case North:
		      if(getDroneAt(x-1,y) == null && x-1 > -1)
		       {
			      result = true;
		      }else {
			      result = false;
		      }
		      break;
	
	      case East:
		      if(getDroneAt(x,y+1) == null && y+1 < arenaY ) {
			      result = true;
		      }else {
			      result = false;
		      }
		      break;
	  
	      case South:
	    	  if(getDroneAt(x+1,y) == null && x+1 < arenaX) {
			      result = true;
		      }else {
			      result = false;
		      }
	    	  break;
	    	  
	      case West:
	    	  if(getDroneAt(x,y-1) == null && y-1 > -1) {
			      result = true;
		      }else {
			      result = false;
		      }
	    	  break;
	      default:
	    	  System.out.println("error");
	  }
	return result;
	  
  }
 
  public String getAllDrones() {
	  String coordinates = "";
	  for(Drone element : Drones) {
		  coordinates = coordinates + element.getX() + " " + element.getY() + " " + "\n";
	  }
	  return coordinates;
  }
  
  public static Drone getDroneAt(int x, int y) {
	  for(Drone element : Drones) {
		  if(element.getX() == x && element.getY() == y) {
			  return element;
		  }
		  
	  }
	  return null;
	  
  }
  
  public int getX() {
	   return this.x;
  }
  
  public int getY() {
	   return this.y;
  }
  
 
  public String toString() {
	  String res = "Arena size is x = " + this.x + " y = " + this.y + "\n";
	  for(Drone element : Drones)
		  res = res + element + "\n";
	  
	  return res;
	  
  }
}
